#ifndef IN_OUT
#define IN_OUT

#include <stdio.h>

enum status {
    SUCCESS = 0,
    FAIL = 1
};

enum status read_file(FILE** file, char* filename);
enum status write_file(FILE** file, char* filename);

#endif
