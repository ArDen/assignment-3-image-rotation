#ifndef ROTATE
#define ROTATE

#include "img.h"

/* создаёт копию изображения, которая повёрнута на 'angle' градусов */
struct image rotate( struct image source, long angle );

#endif
