#include "bmp.h"

uint8_t get_padding(const uint64_t width) {
    return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header = {0};

    // read header
    size_t was_read = fread(&header, sizeof(struct bmp_header), 1, in);

    // check if header was read
    if (!was_read) {
        return READ_INVALID_HEADER;
    }

    // check signature
    if (header.bfType != 0x4D42) {
        return READ_INVALID_SIGNATURE;
    }

    // check pixels
    if (header.biBitCount != 24) {
        return READ_INVALID_BITS;
    }

    // create image
    img -> width = header.biWidth;
    img -> height = header.biHeight;
    img -> data = malloc(sizeof(struct pixel) * header.biHeight * header.biWidth);

    // check that it was created
    if (img -> data == NULL) {
        return READ_INVALID_BITS;
    }

    const uint8_t padding = get_padding(img -> width);

    // read file
    for (size_t i = 0; i < img -> height; i++) {
        if (!fread(img -> data + i * img -> width, sizeof(struct pixel), img -> width, in)) {
            free(img -> data);
            return READ_INVALID_BITS;
        } else {
            fseek(in, padding, SEEK_CUR);
        }
    }

    return READ_OK;
}

struct bmp_header fill_bmp_header(struct image const* img) {
    struct bmp_header header = {0};

    const uint8_t padding = get_padding(img -> width);

    header.bfType = 0x4D42;
    header.bfileSize = sizeof(struct bmp_header) + img -> height * (img -> width * sizeof(struct pixel) + padding);
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = 40;
    header.biHeight = img -> height;
    header.biWidth = img -> width;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = img -> height * (img -> width * sizeof(struct pixel) + padding);
    header.biXPelsPerMeter = 1;
    header.biYPelsPerMeter = 1;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    return header;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    struct bmp_header header = fill_bmp_header(img);

    const uint8_t padding = get_padding(img -> width);

    // write header
    size_t was_write = fwrite(&header, sizeof(struct bmp_header), 1, out);
    if (!was_write) {
        return WRITE_ERROR;
    }

    //write file
    for (size_t i = 0; i < img -> height; i++) {
        if (!fwrite(img -> data + i * img -> width, sizeof(struct pixel), img -> width, out)) {
            return WRITE_ERROR;
        } else {
            fseek(out, padding, SEEK_CUR);
        }
    }

    return WRITE_OK;
}
