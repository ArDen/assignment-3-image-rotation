#include "in_out.h"

enum status read_file(FILE** file, char* filename) {

    if (filename == NULL) {
        return FAIL;
    }

    *file = fopen(filename, "rb");

    if (*file == NULL) {
        return FAIL;
    }

    return SUCCESS;
}

enum status write_file(FILE** file, char* filename) {

    if (filename == NULL) {
        return FAIL;
    }

    *file = fopen(filename, "wb");

    if (*file == NULL) {
        return FAIL;
    }

    return SUCCESS;
}
