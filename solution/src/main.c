#include "bmp.h"
#include "img.h"
#include "in_out.h"
#include "rotate.h"


int main( int argc, char** argv ) {
    if (argc != 4) {
        fprintf(stderr, "Input must be only 3 args");
        return 1;
    }

    char* end_ptr;
    long angle = strtol(argv[3], &end_ptr, 10);

    if (angle % 90 != 0 || angle < -270 || angle > 270) {
        fprintf(stderr, "Wrong angle, you can input only one from this: [0 ,90, -90, 190, -180, 270, -270]");
        return -1;
    }

    char* in_name = argv[1];
    char* out_name = argv[2];
    FILE* in;

    enum status read_status = read_file(&in, in_name);
    if (read_status != SUCCESS) {
        fprintf(stderr, "Can't open input file %s", in_name);
        return 1;
    }

    struct image img = {0};
    enum read_status read_bmp_status = from_bmp(in, &img);
    fclose(in);

    if (read_bmp_status != READ_OK) {
        return 0;
    }

    struct image res = {0};
    res = rotate(img, angle);

    if (img.data != NULL) {
        free(img.data);
    }

    FILE* out;
    enum status write_status = write_file(&out, out_name);

    if (write_status != SUCCESS) {
        fprintf(stderr, "Can't open output file %s", out_name);
        return 1;
    }

    enum write_status write_bmp_status = to_bmp(out, &res);
    fclose(out);

    if (res.data != NULL) {
        free(res.data);
    }

    if (write_bmp_status != WRITE_OK) {
        return 0;
    }

    return 0;
}
