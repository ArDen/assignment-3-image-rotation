#include "img.h"

void create_rotate_image(struct image* img, uint64_t width, uint64_t height) {
    img -> width = height;
    img -> height = width;
    img -> data = malloc(sizeof(struct pixel) * height * width);
}

struct image rotate_90degree(struct image const source) {
    struct image res = {0};
    create_rotate_image(&res, source.width, source.height);

    if (res.width != 0 && res.height != 0 && source.data != NULL) {
        for (size_t i = 0; i < res.height; i++) {
            for (size_t j = 0; j < res.width; j++) {
                *(res.data + (res.width * i + j)) = *(source.data + (source.width * (j + 1) - (i + 1)));
            }
        }
    }

    return res;
}

struct image rotate(struct image source, long angle) {
    struct image res = {0};

    const uint64_t height = source.width;
    const uint64_t width = source.height;
    create_rotate_image(&res, width, height);

    if (res.width != 0 && res.height != 0 && source.data != NULL) {
        for (size_t i = 0; i < source.height * source.width; i++) {
            *(res.data + i) = *(source.data + i);
        }

        size_t rotation = angle >= 0 ? angle / 90 : angle / 90 + 4;

        for (size_t i = 0; i < rotation; i++) {
            struct image tmp = rotate_90degree(res);
            free(res.data);
            res = tmp;
        }
    }
    return res;
}
